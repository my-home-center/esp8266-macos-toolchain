#!/bin/bash

BASEDIR="$HOME/Projects/ESP8266-Toolchain"

# DO NOT CHANGE ANYTHING AFTER THIS LINE


## v1.3.0 : http://bbs.espressif.com/viewtopic.php?f=46&t=919
NEW_SDK_URL="http://bbs.espressif.com/download/file.php?id=664"


TMPDIR="$HOME/Downloads/esp-tmp"

echo '*' Creating Temp directory...
mkdir -p "$TMPDIR" 2>>/dev/null
cd "$TMPDIR"

echo '*' Downloading...

echo ' ->' New Espressif SDK
curl -#Lo 'new_sdk.zip' $NEW_SDK_URL

echo '*' Extracting archives...

echo ' ->' New Espressif SDK
cd "$TMPDIR"; unzip -q "$TMPDIR/new_sdk.zip"
NEW_SDK_DIR=`find "$TMPDIR" -d 1 -name 'esp_iot_sdk*' | head -n 1`
NEW_SDK_DIR=${NEW_SDK_DIR##*/}
mv "$NEW_SDK_DIR" "$BASEDIR"
mv License "$BASEDIR"
mv release_note.txt "$BASEDIR"
cd "$BASEDIR"; ln -fhs "$NEW_SDK_DIR" SDK

echo '*' Cleanup...
cd "$HOME"
rm -rf "$TMPDIR"
rm -rf "$BASEDIR/$NEW_SDK_DIR/document"
rm -rf "$BASEDIR/$NEW_SDK_DIR/examples"
rm -rf "$BASEDIR/$NEW_SDK_DIR/Makefile"
#rm -rf "$BASEDIR/$NEW_SDK_DIR/tools"
rm -rf "$BASEDIR/$NEW_SDK_DIR/app"

echo '*' All Done!
echo
echo Good Luck and Happy Coding!
echo
