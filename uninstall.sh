#!/bin/bash

BASEDIR="$HOME/Projects/ESP8266-Toolchain"

# DO NOT CHANGE ANYTHING AFTER THIS LINE




# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

cat "$BASEDIR/unpyserial.log" | sudo xargs rm -rf
rm -rf '/Applications/Eclipse.app'
rm -rf "$BASEDIR"

echo
echo Uninstallation done!
echo
echo Please realize that this script will not uninstall JDK and
echo XCode Command Line Tools, please do it yourself if needed!
echo