#!/bin/bash

BASEDIR="$HOME/Projects/ESP8266-Toolchain"

# DO NOT CHANGE ANYTHING AFTER THIS LINE

ECLIPSE_URL="https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/mars/R/eclipse-cpp-mars-R-macosx-cocoa-x86_64.tar.gz&mirror_id=1"
TOOLCHAIN_URL="https://bitbucket.org/my-home-center/esp8266-macos-toolchain/downloads/xtensa-lx106-standalone-y.zip"
# v1.1.2
SDK_URL="http://bbs.espressif.com/download/file.php?id=521"
SDK_PATCH1_URL="http://bbs.espressif.com/download/file.php?id=525"
SDK_PATCH2_URL="http://bbs.espressif.com/download/file.php?id=539"
SDK_PATCH3_URL="http://bbs.espressif.com/download/file.php?id=546"
#PYSERIAL_URL="http://sourceforge.net/projects/pyserial/files/pyserial/2.7/pyserial-2.7.tar.gz/download"
PYSERIAL_URL="https://bitbucket.org/my-home-center/esp8266-macos-toolchain/downloads/pyserial-2.7.tar.gz"
EX_MAKEFILE_URL="https://bitbucket.org/my-home-center/esp8266-macos-toolchain/downloads/Makefile.example"

TMPDIR="$HOME/Downloads/esp-tmp"

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.osx` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

echo '*' Creating Projects directory...
mkdir -p "$BASEDIR" 2>>/dev/null
mkdir -p "$TMPDIR" 2>>/dev/null

cd "$TMPDIR"

echo '*' Downloading tools...

echo ' ->' Eclipse
curl -#Lo 'eclipse.tgz' $ECLIPSE_URL

echo ' ->' Compiled Toolchain
curl -#Lo 'xtensa-lx106.zip' $TOOLCHAIN_URL

echo ' ->' Espressif SDK and Patches
curl -#Lo 'sdk.zip' $SDK_URL
curl -#Lo 'patch1.zip' $SDK_PATCH1_URL
curl -#Lo 'patch2.zip' $SDK_PATCH2_URL
curl -#Lo 'patch3.zip' $SDK_PATCH3_URL

echo ' ->' PySerial
curl -#Lo 'pyserial.tgz' $PYSERIAL_URL

echo ' ->' Example Makefile
curl -#Lo "$BASEDIR/Makefile.example" $EX_MAKEFILE_URL

echo '*' Extracting archives...

echo ' ->' Eclipse
cd /Applications; tar -zxf "$TMPDIR/eclipse.tgz"

echo ' ->' Toolchain
unzip -q "$TMPDIR/xtensa-lx106.zip" -d "$BASEDIR"

echo ' ->' Espressif SDK and Patches
unzip -q "$TMPDIR/sdk.zip" -d "$BASEDIR"
SDK_DIR=`find "$BASEDIR" -d 1 -name 'esp_iot_sdk*' | head -n 1`
SDK_DIR=${SDK_DIR##*/}
cd "$BASEDIR"
ln -fhs "$SDK_DIR" SDK
#echo found SDK dir: "$BASEDIR/$SDK_DIR"
unzip -qo "$TMPDIR/patch1.zip" -d "$BASEDIR/SDK/lib"
unzip -qo "$TMPDIR/patch2.zip" -d "$BASEDIR/SDK/lib"
unzip -qo "$TMPDIR/patch3.zip" -d "$BASEDIR/SDK/lib"

echo ' ->' PySerial
cd "$TMPDIR"; tar -xzf pyserial.tgz
PYSERIAL_DIR=`find "$TMPDIR" -d 1 -name 'pyserial*' | head -n 1`
cd "$PYSERIAL_DIR"
sudo python setup.py install --record "$BASEDIR/unpyserial.log" &>/dev/null

echo '*' Modyfing Example Makefile
sed -i '' 's,XXXBASEDIRXXX,'"$BASEDIR"',' "$BASEDIR/Makefile.example"

echo '*' Cleanup...
cd "$HOME"
sudo rm -rf "$TMPDIR"
rm -rf "$BASEDIR/SDK/document"
rm -rf "$BASEDIR/SDK/examples"
rm -rf "$BASEDIR/SDK/Makefile"
rm -rf "$BASEDIR/SDK/tools"
rm -rf "$BASEDIR/SDK/app"

echo '*' All Done!
echo
echo Do not forget to install the latest JDK and Xcode CommandLine Tools
javac 2>>/dev/null
xcode-select --install &>/dev/null
echo
echo To compile firmware please check Toolchain and SDK paths in Makefile
echo example file you can find at
echo "$BASEDIR/Makefile.example"
echo
echo If you do not want to use Eclipse, than consider to add toolchain to system path
echo "$BASEDIR/xtensa-lx106-elf/bin"
echo
echo Good Luck and Happy Coding!
echo
echo