# ESP8266 MacOS Toolchain - автоустановка утилит #

Проект - скрипты автоматической установки/обновления/удаления toolchain для написания 
и прошивки ESP8266.

## Установка toolchain

1. Скачать файл install.sh
2. Проверить переменную `BASEDIR` в скрипте (сюда будет установлен toolchain)
3. Запустить скрипт

		#!bash
		
		chmod +x install.sh
		./install.sh

## Обновление SDK

1. Скачать файл update_sdk.sh
2. Проверить переменную `BASEDIR` в скрипте (сюда будет установлен обновленный SDK)
3. Запустить скрипт

		#!bash
		
		chmod +x update_sdk.sh
		./update_sdk.sh

## Удаление toolchain

1. Скачать файл uninstall.sh
2. Проверить переменную `BASEDIR` в скрипте (эта директория будет удалена)
3. Запустить скрипт

		#!bash
		
		chmod +x uninstall.sh
		./uninstall.sh
