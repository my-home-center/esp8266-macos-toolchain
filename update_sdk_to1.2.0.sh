#!/bin/bash

BASEDIR="$HOME/Projects/ESP8266-Toolchain"

# DO NOT CHANGE ANYTHING AFTER THIS LINE



# v1.2.0 : http://bbs.espressif.com/viewtopic.php?f=5&t=680
NEW_SDK_URL="http://bbs.espressif.com/download/file.php?id=564"
# libsmartconfig patch : http://bbs.espressif.com/viewtopic.php?f=5&t=716
NEW_SDK_PATCH1="http://bbs.espressif.com/download/file.php?id=585"
# libssl patch : http://bbs.espressif.com/viewtopic.php?f=5&t=708
NEW_SDK_PATCH2="http://bbs.espressif.com/download/file.php?id=583"
# libsdram http://bbs.espressif.com/viewtopic.php?f=5&t=734
NEW_SDK_PATCH3="http://bbs.espressif.com/download/file.php?id=594"


TMPDIR="$HOME/Downloads/esp-tmp"

echo '*' Creating Temp directory...
mkdir -p "$TMPDIR" 2>>/dev/null
cd "$TMPDIR"

echo '*' Downloading...

echo ' ->' New Espressif SDK
curl -#Lo 'new_sdk.zip' $NEW_SDK_URL

echo ' ->' Patch 1: Lib SmartConfig
curl -#Lo 'libsmartconfig.zip' $NEW_SDK_PATCH1

echo ' ->' Patch 2: Lib SSL
curl -#Lo 'libssl.zip' $NEW_SDK_PATCH2

echo ' ->' Patch 3: SDRAM Memory Optimization
curl -#Lo 'sdram.zip' $NEW_SDK_PATCH3

echo '*' Extracting archives...

echo ' ->' New Espressif SDK
cd "$TMPDIR"; unzip -q "$TMPDIR/new_sdk.zip"
NEW_SDK_DIR=`find "$TMPDIR" -d 1 -name 'esp_iot_sdk*' | head -n 1`
NEW_SDK_DIR=${NEW_SDK_DIR##*/}
mv "$NEW_SDK_DIR" "$BASEDIR"
mv License "$BASEDIR"
mv release_note.txt "$BASEDIR"
cd "$BASEDIR"; ln -fhs "$NEW_SDK_DIR" SDK

echo ' ->' Patch 1: Lib SmartConfig
unzip -qo "$TMPDIR/libsmartconfig.zip" -d "$BASEDIR/SDK/lib"

echo ' ->' Patch 2: Lib SSL
unzip -qo "$TMPDIR/libssl.zip" -d "$BASEDIR/SDK/lib"

echo ' ->' Patch 3: SDRAM Memory Optimization
unzip -qo "$TMPDIR/sdram.zip" -d "$BASEDIR/SDK/lib"

echo '*' Cleanup...
cd "$HOME"
rm -rf "$TMPDIR"
rm -rf "$BASEDIR/$NEW_SDK_DIR/document"
rm -rf "$BASEDIR/$NEW_SDK_DIR/examples"
rm -rf "$BASEDIR/$NEW_SDK_DIR/Makefile"
rm -rf "$BASEDIR/$NEW_SDK_DIR/tools"
rm -rf "$BASEDIR/$NEW_SDK_DIR/app"

echo '*' All Done!
echo
echo Good Luck and Happy Coding!
echo
